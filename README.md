# cnode-vue

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

项目结构：

| 目录            | 说明           |
| :-------------: |:-------------:|
| api             | 接口请求       |
| assets          | 静态资源       |
| components      | 公共组件       |
| filters         | 过滤器         |
| constants       | 常量           |
| directives      | 自定义指令     |
| mixins          | 混合           |
| router          | 路由           |
| store           | vuex状态管理   |
| utils           | 工具库         |
| views           | 页面           |
