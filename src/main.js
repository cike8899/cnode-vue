// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import request from './api';
import './assets/css/iconfont.css';

import { Button, Select, Pagination, Message } from 'element-ui';

import AuthorMessage from './components/authorMessage';

Vue.config.productionTip = false;

const components = [Button, Select, Pagination];

components.map(x => {
  Vue.component(x.name, x);/* 全局注册示例 */
});

Vue.component(AuthorMessage.name, AuthorMessage)/* 全局注册,作者信息组件注册处 */

Object.defineProperties(Vue.prototype, {
  $http: {
    value: request,
    writable: false
  },
  $message: {
    value: Message,
    writable: false
  }
});
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  data: {
    eventBus: new Vue()
  },
  template: '<App/>',
  components: { App }
});
