import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Home from '@/views/Home'
import detailsPage from '@/views/detailsPage'
import contentSubmit from '@/views/contentSubmit'
import gettingStarted from '@/views/gettingStarted'
import about from '@/views/about'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/hello',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/detailsPage/:id',/* 带出id,路径显示在地址栏路径什么样地址栏就显示什么样 */
      name: 'detailsPage',
      component: detailsPage
    },
    {
      path: '/contentSubmit',
      name: 'contentSubmit',
      component: contentSubmit
    },
    {
      path: '/gettingStarted',
      name: 'gettingStarted',
      component: gettingStarted
    },
    {
      path: '/about',
      name: 'about',
      component: about
    },
  ]
})
