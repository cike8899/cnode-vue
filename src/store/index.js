import Vue from 'vue';
import Vuex from 'vuex'
import * as actions from './actions.js'
import * as getters from './getters'

import cart from './modules/cart'



Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  actions,
  getters,
  modules: {
    cart
  },
  strict: debug,
});
