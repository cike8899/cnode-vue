import * as types from '../mutation-types'

// initial state
const state = {
  userInfo: {},
  moduleInfo: {}
}

// getters
const getters = {
  userInfo: state => state.userInfo,
  moduleInfo: state => state.moduleInfo
}

// actions
const actions = {
  [types.CLEAR_ALL_STATE]({ state, commit, rootState }) {
    commit(types.CLEAR_ALL_STATE, rootState);
  }
}

// mutations
const mutations = {
  [types.SET_USERINFO](state, info) {
    state.userInfo = info
  },
  [types.SET_MODULE](state, moduleInfo) {
    state.moduleInfo = moduleInfo;
  },
  [types.CLEAR_ALL_STATE](state, rootState) {
    Object.keys(rootState).forEach(x => {
      rootState[x] = {};
    });
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

