//state
const state = {
        wrongListFilter: null,
        /*错题本筛选*/
        wrongTopicDetail: {},/*错题详情参数*/

       /* currentTopicNo:null,*/
        // wrongCardParam:{},/*查看答题卡参数*/
    }
    // mutations
const mutations = {
        SET_WRONGLISTFILTER(state, data) {
            state.wrongListFilter = data
        },
        SET_WRONGTOPICDETAIL(state, data) {
            state.wrongTopicDetail = data
        },
       /* SET_CURRENTTOPICDETAIL(state, data){
            state.currentTopicNo = data
        },*/
        SET_WRONGCARDPARAM(state,data){
            state.wrongCardParam = data
        }
    }
    // getters
const getters = {
    wrongTopicDetail(state, getters) {
        const { wrongTopicDetail } = state;
        return wrongTopicDetail;
    },
   /* wrongCardParam(state, getters) {
        const { wrongCardParam } = state;
        return wrongCardParam;
    },*/
}

// actions
const actions = {

}

export default {
    state,
    getters,
    actions,
    mutations
}
