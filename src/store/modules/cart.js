import * as types from '../mutation-types'

// initial state
// shape: [{ id, quantity }]
const state = {
  added: [],
  checkoutStatus: null
}

// getters
const getters = {
  checkoutStatus: state => state.checkoutStatus
}

// actions
const actions = {
  checkout({ commit, state }, { id }) {
    console.info("actions", id);
    commit(types.CHECKOUT_REQUEST, { id });
  }
}

// mutations
const mutations = {
  [types.CHECKOUT_REQUEST](state, { id }) {
    // clear cart
    console.info("mutations", id);
    state.added = []
    state.checkoutStatus = null
  },
  [types.ADD_TO_CART](state, { id }) {
    state.lastCheckout = null
    const record = state.added.find(p => p.id === id)
    if (!record) {
      state.added.push({
        id,
        quantity: 1
      })
    } else {
      record.quantity++
    }
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}

