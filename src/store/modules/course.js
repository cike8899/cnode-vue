import * as types from '../mutation-types'

// initial state
const state = {
  sectionInfo: {},//小节
  practiceType: 2
}

// getters
const getters = {
  sectionInfo: state => state.sectionInfo,
  practiceType: state => state.practiceType
}

// actions
const actions = {

}

// mutations
const mutations = {
  [types.SET_SECTIONINFO](state, info) {
    state.sectionInfo = info
  },
  [types.SET_PRACTICETYPE](state, type) {
    state.practiceType = type
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

