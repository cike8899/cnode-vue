//state
const state = {
    honorChangeState: null,/*荣誉榜状态*/
    honorRollStyle:null,/*视频，刷题，进步tab*/
}
    // mutations
const mutations = {
    SET_HONORCHANGESTATE(state, data){
    	state.honorChangeState = data
    },
    SET_HONORROLLSTYLE(state, data){
        state.honorRollStyle = data
    },
}
    // getters
const getters = {

}

// actions
const actions = {

}

export default {
    state,
    getters,
    actions,
    mutations
}
